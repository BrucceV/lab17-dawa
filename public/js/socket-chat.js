var socket = io();
var params = new URLSearchParams(window.location.search);

const messageContainer = document.getElementById('message-container')
const messageForm = document.getElementById('send-container')
const messageInput = document.getElementById('message-input')
const userContainer = document.getElementById('user-container')
const userListContainer = document.getElementById('list-container')

const date = new Date();
var hours = date.getHours();
var minutes = date.getMinutes();
var ampm = hours >= 12 ? 'pm' : 'am';
hours = hours % 12;
hours = hours ? hours : 12; // the hour '0' should be '12'
minutes = minutes < 10 ? '0'+minutes : minutes;
var strTime = hours + ':' + minutes + ' ' + ampm;

//const name = prompt('What is your name?')
/* const name = prompt('Cual es tu nombre?');
appendMessage('Entraste a la sala');
appendMessageUser(name) */
//appendMessageListUser(name)
socket.emit('new-user', params.get('nombre'));

if (!params.has('nombre') || !params.has('sala')) {
    window.location = 'index.html';
    throw new Error('El nombre y sala son necesarios');
}

var usuario = {
    nombre: params.get('nombre'),
    sala: params.get('sala')
};

socket.on("connect", function() {
    console.log("Conectado al servidor");

    socket.emit("entrarChat", usuario, function (resp) {
        console.log("Usuarios conectados", resp);
        renderizarUsuarios(resp)
    });
});

socket.on("disconnect", function () {
    console.log("Perdimos conexion con el servidor");
});

socket.on("crearMensaje", function (mensaje) {
    console.log("servidor mensaje", mensaje);
    renderizarMensajes(mensaje, false)
});

socket.on("listaPersonas", function(personas) {
    console.log("listaPersonas -----> ", personas);
    renderizarUsuarios(personas)
});

// Mensajes privados
socket.on('mensajePrivado', function(mensaje) {
    console.log('Mensaje Privado:', mensaje);
});

socket.on('user-connected', name => {
/*     console.log('user connected:', name);
 */    appendMessage(`${name} se unio a la sala`)
})

socket.on('user-disconnected', name => {
    console.log('user disconnected:', name);
    appendMessage(`${name} se fue de la sala`)
})
