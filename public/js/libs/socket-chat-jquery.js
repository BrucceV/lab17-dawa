var params = new URLSearchParams(window.location.search);

var nombre = params.get("nombre");
var sala = params.get("sala");

//referencias de jquery

var divUsuarios = $("#divUsuarios");

var formEnviar = $("#formEnviar");

var txtMensaje = $("#txtMensaje");

var divChatbox = $("#divChatbox");

//funciones para renderizar usuarios

function renderizarUsuarios(personas) {
    var html = new Array();

    html += "<div>";
    html +=
        '<h4 href="javascript:void(0)" class="active"> Chat de <span>' +
        params.get("sala") +
        "</span></h4>";
    html += "</div>";

    for (var i=0; i < personas.length; i++){
        //console.log(personas[i].id)

        html += '<div class="chat_people">';
        html +=
            '<div class="chat_img" data-id="' +
            personas[i].id +
            '" href="javascript:void(0)"><img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"></div><div class="chat_ib"><h5>' +
            personas[i].nombre +
            '<span class="chat_date">Online</span></h5></div>';
        html += "</div>";
    }
    divUsuarios.html(html);
 }

function renderizarMensajes(mensaje, yo) {
    //console.log("Fecha renderizar: "+mensaje.fecha)
    var html = "";

    var fecha = new Date(mensaje.fecha);
    var hora = fecha.getHours() + ":" + fecha.getMinutes();

    var day = new Date().getTime();
    var date = new Date(day);
    var time = date.getHours() + ":" + date.getMinutes();

    if(yo) {
        console.log("renderizarMensajes: yo")
        html += '<div class="outgoing_msg">';
        html += '   <div class="sent_msg">';
        html += '       <div class="">';
        html += '           <p>' + mensaje.mensaje + '</p>';
        html += '           <span class="time_date">' + time + ' | ' + mensaje.nombre + '</span>';
        html += '       </div>';
        html += '   </div>';
        html += '</div>';
    } else {
        console.log("renderizarMensajes: el")
        html += '<div class="incoming_msg">';
        html += '   <div class="incoming_msg_img">'
        html += '       <img src="https://ptetutorials.com/images/user-profile.png"alt="sunil">';
        html += "   </div>";
        html += '   <div class="received_msg"> <div class="received_withd_msg"> <p>'+ mensaje.mensaje + '</p> <span class="time_date"> ' + time + ' | ' + mensaje.nombre + ' </span> </div> </div>';
        html += "</div>";
    }

    divChatbox.append(html);
}

function appendMessage(mensaje) {
    var day = new Date().getTime();
    var date = new Date(day);
    var time = date.getHours() + ":" + date.getMinutes();

/*     console.log("appendMessage:");
    console.log(mensaje); */
    var html = new Array();

    html += '<div class="incoming_msg">';
    html += '   <div class="received_msg"> <div class="received_withd_msg"> <p>'+ mensaje + '</p> <span class="time_date"> ' + time + ' | Sistema </span> </div> </div>';
    html += "</div>";

    divChatbox.append(html);
}
//Listeners

divUsuarios.on("click", "a", function () {
    var id = $(this).data("id");

    console.log(id);
});

formEnviar.on('submit', function (e) {
    e.preventDefault();
    // console.log(txtMensaje.val().trim());

    if (txtMensaje.val().trim() === 0) {
      return;
    }
    function chat(mensaje) {
      txtMensaje.val('').focus();
      renderizarMensajes(mensaje, true);
    }
    let mensaje = {
      nombre: nombre,
      mensaje: txtMensaje.val(),
    };
    socket.emit('crearMensaje', mensaje, chat(mensaje));
});
